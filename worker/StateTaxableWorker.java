package worker;

public class StateTaxableWorker extends TaxableWorker {
	public String statename;public float statetaxrate;

	public StateTaxableWorker(String name, float federaltaxrate, float PayRate, String statename, float statetaxrate) {
		super(name, federaltaxrate, PayRate);
		this.statename = statename;
		this.statetaxrate = statetaxrate;
	}

	public float getStatetaxrate() {
		return statetaxrate;
	}

	public void setStatetaxrate(float statetaxrate) {
		this.statetaxrate = statetaxrate;
	}

	@Override
	public String toString() {
		return "StateTaxableWorker [statename=" + statename + ", statetaxrate=" + statetaxrate + ", name=" + name
				+ ", federaltaxrate=" + federaltaxrate + ", PayRate=" + PayRate + ", hrs=" + hrs + ", grossPayamount="
				+ grossPayamount + "]";
	}
	@Override
	public double taxWithheld(double grossPayamount)
	{
		
		double taxheld=grossPayamount*federaltaxrate;
		
		double statetaxheld=grossPayamount*statetaxrate;
		double staxheld = statetaxheld+taxheld;
		return staxheld;
	}

}
